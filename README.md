# The weird noise in the office

This [repository](https://codeberg.org/penguinsfly/weird-noise-viz) contains the data and code for inpsecting the weird noise in the office.

The accompanying blog is at [penguinsfly.club](https://www.penguinsfly.club/rambling/weird_noise)

## Requirements

The data are in `data`.

The notebook can be run with `jupyterlab` and/or `jupyternotebook`

The `python-pip` requirements are:

```
numpy
pandas
librosa
scipy
sklearn
tqdm
matplotlib
seaborn
```

To install everything, create a virtual environment then install:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Jupyter-book

To produce a `jupyter-book` page, this is required, and can be installed with `pip`:

```
jupyter-book
```

The configuration is set to no execution.

### Local build

The page can be produced with:

```bash
jupyterbook build .
```

### Publish via local repo

```bash
git switch --orphan pages
```
